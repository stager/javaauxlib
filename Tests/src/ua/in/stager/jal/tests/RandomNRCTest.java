package ua.in.stager.jal.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import ua.in.stager.jal.RandomNRC;

public class RandomNRCTest
 {
  @Test
  public void testNextLongStartSeed42 ()
   {
    RandomNRC rng = new RandomNRC(42);

    assertEquals(2235175048639730301L,rng.nextLong());
    assertEquals(6425562075534813739L,rng.nextLong());
    assertEquals(3657314841840734556L,rng.nextLong());
    assertEquals(-9011764187247975270L,rng.nextLong());
    assertEquals(1943253282200294373L,rng.nextLong());
    assertEquals(7151416262339826735L,rng.nextLong());
    assertEquals(5714412476670365920L,rng.nextLong());
    assertEquals(-8384457932508490741L,rng.nextLong());
    assertEquals(3696497600906511325L,rng.nextLong());
    assertEquals(6974551640960948115L,rng.nextLong());
   }

  @Test
  public void testNextLongStartSeed3141592653589 ()
   {
    RandomNRC rng = new RandomNRC(3141592653589L);

    assertEquals(2712999236716032656L,rng.nextLong());
    assertEquals(-7735964294722831198L,rng.nextLong());
    assertEquals(542335164592990219L,rng.nextLong());
    assertEquals(-5059150507683547268L,rng.nextLong());
    assertEquals(9146449068200026519L,rng.nextLong());
    assertEquals(-6183273892470242156L,rng.nextLong());
    assertEquals(2606812531830912225L,rng.nextLong());
    assertEquals(8052825225420452580L,rng.nextLong());
    assertEquals(8947631858922499302L,rng.nextLong());
    assertEquals(-1272026377456793143L,rng.nextLong());
    assertEquals(-218361492629574726L,rng.nextLong());
    assertEquals(-4707304675225466276L,rng.nextLong());
    assertEquals(-49213892730047580L,rng.nextLong());
    assertEquals(8790428000886163866L,rng.nextLong());
    assertEquals(-1769326593427490985L,rng.nextLong());
    assertEquals(-7062686296663908484L,rng.nextLong());
    assertEquals(-1981448971492255664L,rng.nextLong());
    assertEquals(8214052863611933416L,rng.nextLong());
    assertEquals(3628057984647660749L,rng.nextLong());
    assertEquals(4562518475448625495L,rng.nextLong());
   }

  @Test
  public void testNextLongInRange ()
   {
    RandomNRC rng = new RandomNRC(42);

    assertEquals(2,rng.nextLongInRange(1,10));
    assertEquals(10,rng.nextLongInRange(1,10));
    assertEquals(7,rng.nextLongInRange(1,10));
    assertEquals(7,rng.nextLongInRange(1,10));
    assertEquals(4,rng.nextLongInRange(1,10));
    assertEquals(6,rng.nextLongInRange(1,10));
    assertEquals(1,rng.nextLongInRange(1,10));
    assertEquals(6,rng.nextLongInRange(1,10));
    assertEquals(6,rng.nextLongInRange(1,10));
    assertEquals(6,rng.nextLongInRange(1,10));
    assertEquals(4,rng.nextLongInRange(1,10));
    assertEquals(7,rng.nextLongInRange(1,10));
    assertEquals(5,rng.nextLongInRange(1,10));
    assertEquals(1,rng.nextLongInRange(1,10));
    assertEquals(9,rng.nextLongInRange(1,10));
    assertEquals(7,rng.nextLongInRange(1,10));
    assertEquals(3,rng.nextLongInRange(1,10));
    assertEquals(5,rng.nextLongInRange(1,10));
    assertEquals(5,rng.nextLongInRange(1,10));
    assertEquals(7,rng.nextLongInRange(1,10));
   }

  @Test
  public void testNextDouble ()
   {
    RandomNRC rng = new RandomNRC(42);

    assertTrue(almostEqual(0.121169,rng.nextDouble()));
    assertTrue(almostEqual(0.348330,rng.nextDouble()));
    assertTrue(almostEqual(0.198263,rng.nextDouble()));
    assertTrue(almostEqual(0.511471,rng.nextDouble()));
    assertTrue(almostEqual(0.105344,rng.nextDouble()));
    assertTrue(almostEqual(0.387679,rng.nextDouble()));
    assertTrue(almostEqual(0.309779,rng.nextDouble()));
    assertTrue(almostEqual(0.545478,rng.nextDouble()));
    assertTrue(almostEqual(0.200388,rng.nextDouble()));
    assertTrue(almostEqual(0.378091,rng.nextDouble()));
    assertTrue(almostEqual(0.132455,rng.nextDouble()));
    assertTrue(almostEqual(0.908610,rng.nextDouble()));
    assertTrue(almostEqual(0.937517,rng.nextDouble()));
    assertTrue(almostEqual(0.822863,rng.nextDouble()));
    assertTrue(almostEqual(0.310137,rng.nextDouble()));
    assertTrue(almostEqual(0.618366,rng.nextDouble()));
    assertTrue(almostEqual(0.352726,rng.nextDouble()));
    assertTrue(almostEqual(0.223118,rng.nextDouble()));
    assertTrue(almostEqual(0.074038,rng.nextDouble()));
    assertTrue(almostEqual(0.044179,rng.nextDouble()));
   }

  @Test
  public void testElementsFrequency ()
   {
    RandomNRC rng = new RandomNRC(42);
    final int n   = 10;
    final int m   = 1000000;
    int[]     a   = new int[n];

    for (int i = 0; i < m; i++)
     {
      a[(int)rng.nextLongInRange(0,n - 1)]++;
     }

    for (int i = 0; i < n; i++)
     {
      assertTrue(almostEqual((double)a[i] / m,1.0 / n,0.01));
     }
   }

  @Test
  public void testSaveRestoreState ()
   {
    RandomNRC rng1 = new RandomNRC(31415926);

    for (int i = 0; i < 1000; i++)
     {
      rng1.nextLong();
     }

    String state = rng1.toStateString();

    RandomNRC rng2 = RandomNRC.fromStateString(state);

    for (int i = 0; i < 1000; i++)
     {
      assertEquals(rng1.nextLong(),rng2.nextLong());
     }
   }

  private static boolean almostEqual (double a, double b, double epsilon)
   {
    return Math.abs(a - b) < epsilon;
   }

  private static boolean almostEqual (double a, double b)
   {
    return almostEqual(a,b,ALMOST_EQUAL_EPSILON);
   }

  private static double ALMOST_EQUAL_EPSILON = 0.000001;
 }

