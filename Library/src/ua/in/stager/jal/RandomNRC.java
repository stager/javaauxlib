package ua.in.stager.jal;

/* Random number generator described in "Numerical recipes in C" 3rd edition */

public class RandomNRC
 {
  public RandomNRC (long seed)
   {
    v = 4101842887655102017L;
    w = 1;

    if (seed == v) // algorithm does not allow seed to be equal to V_INIT
     {
      seed--;
     }

    u = seed ^ v; nextLong();
    v = u;        nextLong();
    w = v;        nextLong();
   }

  private RandomNRC (long u, long v, long w)
   {
    this.u = u;
    this.v = v;
    this.w = w;
   }

  public long nextLong ()
   {
    long x;

    u  = u * 2862933555777941757L + 7046029254386353087L;
    v ^= v >>> 17;
    v ^= v <<  31;
    v ^= v >>>  8;
    w  = 4294957665L * (w & 0xffffffffL) + (w >>> 32);
    x  = u ^ (u << 21);
    x ^= x >>> 35;
    x ^= x << 4;

    return (x + v) ^ w;
   }

  public long nextInt ()
   {
    return (int)(nextLong());
   }

  /* returns random number in [a,b] range */
  public long nextLongInRange (long a, long b)
   {
    long n   = (b - a + 1);
    long v   = nextLong();
    long hi  = (v >> 32) & 0xffffffffL;
    long low = v & 0xffffffffL;

    return a + ((hi % n) * (0x100000000L % n) + (low % n)) % n;
   }

  /* returns random number in [a,b] range */
  public double nextDouble ()
   {
    double v = 5.42101086242752217E-20 * nextLong();

    return v < 0.0 ? 1.0 + v : v;
   }

  public String toStateString ()
   {
    StringBuilder sb = new StringBuilder();

    sb.append(u);
    sb.append(' ');
    sb.append(v);
    sb.append(' ');
    sb.append(w);

    return sb.toString();
   }

  public static RandomNRC fromStateString (String s)
   {
    String[] params = s.split(" ");
    long     u,v,w;

    try
     {
      u = Long.parseLong(params[0]);
      v = Long.parseLong(params[1]);
      w = Long.parseLong(params[2]);
     }
    catch (Throwable e)
     {
      throw new IllegalStateException("invalid rng state",e);
     }

    return new RandomNRC(u,v,w);
   }

  private long u,v,w;
 }

